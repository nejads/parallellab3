%% @author Soroush
%% @doc @todo Add description to channel.

-module(channel).

%% ====================================================================
%% API functions
%% ====================================================================
-export([main/1, initial_state/1]).
-include_lib("./defs.hrl").

%% ====================================================================
%% Internal functions
%% ====================================================================
main(State) ->
  receive
    {request, From, Ref, Request} ->
      {Response, NextState} = loop(State, Request),
      From ! {result, Ref, Response},
      main(NextState)
  end.

%Join
loop(St, {join, Pid}) ->
	case lists:member(Pid, St#channel_st.clients) of 
		false ->
		    NewClientList = St#channel_st.clients ++ [Pid],
		    NewState = St#channel_st{clients=NewClientList},
		    {ok, NewState};
		true -> 
      		{{error, user_already_joined}, St}
  end;


%Leave 
loop(St, {leave, Pid}) ->
	case lists:member(Pid, St#channel_st.clients) of 
		true ->
		    NewClientList = lists:delete(Pid, St#channel_st.clients),
		    NewState = St#channel_st{clients=NewClientList},
		    {ok, NewState};
		false -> 
			{{error, user_not_joined}, St}
  	end;

%Send
loop(St, {msgFromClient, Pid, Nickname, Msg}) ->
	case lists:member(Pid, St#channel_st.clients) of 
		true ->
			ListToSend = lists:delete(Pid, St#channel_st.clients),	%delete sender's Pid
			sentToOtherClients(ListToSend, {St#channel_st.name, Nickname, Msg}),
			{ok, St};
		false ->
      		{{error, user_not_joined}, St}
 	 end.

%%Private methods, Using tail recursion

sentToOtherClients([], _) ->
	ok;

sentToOtherClients([SendTo|ClientList], {Channel, Nickname, Msg}) ->
	helper:requestAsync(SendTo, {incoming_msg, Channel, Nickname, Msg}),
	sentToOtherClients(ClientList, {Channel, Nickname, Msg}).

initial_state(Channel) ->
    #channel_st{clients=[], name=Channel}.
    

