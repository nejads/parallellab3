-module(server).
-export([main/1, initial_state/1]).
-include_lib("./defs.hrl").

main(State) ->	%It's like main in client. 
    receive
        {request, From, Ref, Request} ->
            {Response, NextState} = loop(State, Request),
            From ! {result, Ref, Response},
            main(NextState)
    end.

%%Produce initial state
initial_state(Server) ->
    #server_st{name=Server,clients=[], channels=[], nicknames=[]}.
    
%Connect
loop(St, {connect, Pid, Nickname}) ->
	case 
		catch lists:member(Nickname, St#server_st.nicknames) of	
		true -> %Nickname have been taken
            {{'EXIT', {error, Nickname, "Nickname is already taken"}}, St};
        false -> 
			NewClientList = St#server_st.clients ++ [Pid],    
            NewNicknameList = St#server_st.nicknames ++ [Nickname],
            NewState = St#server_st{clients=NewClientList, nicknames=NewNicknameList},
            {ok, NewState}
	end;

%Disconnect
loop(St, {disconnect, Pid, Nickname}) ->
    NewClientList = lists:delete(Pid, St#server_st.clients),
    NewNicknameList = lists:delete(Nickname, St#server_st.nicknames),
    NewState = St#server_st{clients=NewClientList, nicknames=NewNicknameList},
    {ok, NewState};

%Join chatroom 
loop(St, {join, Channel, Pid}) ->
	ChannelExists = lists:member(Channel, St#server_st.channels),
	if	
		ChannelExists == false ->	
			NewChannelList = St#server_st.channels ++ [Channel],
			NewState = St#server_st{channels=NewChannelList},
			helper:start(list_to_atom(Channel), channel:initial_state(Channel), 
                    fun channel:main/1),
			helper:requestAsync(list_to_atom(Channel), {join, Pid}),
			{ok, NewState};
		true ->			
			helper:requestAsync(list_to_atom(Channel), {join, Pid}),
			{ok, St}	
	end;

% Ping
loop(St, {ping, PidPingSender, NicknameToPing, PingTimeStamp}) ->
	case lists:member(NicknameToPing, St#server_st.nicknames) of	
		true -> 
			ListToSend = St#server_st.clients, 
			broadcastPingToClients(ListToSend, {PidPingSender, NicknameToPing, PingTimeStamp}),
			{ok, St};
        false -> 
			{{error, user_not_found}, St}
	end.

%Pong
%% loop(St, {pong, PidPingSender, NicknamePingSender, NicknamePongSender}) ->
%% 	case lists:member(NicknamePingSender, St#server_st.nicknames) of	
%% 		true -> 
%% 			helper:requestAsync(PidPingSender, {pong, NicknamePongSender}),
%% 			{ok, St};
%%         false -> 
%% 			{{error, user_not_found}, St}
%% 	end.

%Private methods, using tail recursion
broadcastPingToClients([], _) ->
	ok;

broadcastPingToClients([SendTo|ClientList], {PidPingSender, NicknameToPing, PingTimeStamp}) ->
	helper:requestAsync(SendTo, {sendPong, PidPingSender, NicknameToPing, PingTimeStamp}),
	broadcastPingToClients(ClientList, {PidPingSender, NicknameToPing, PingTimeStamp}).
