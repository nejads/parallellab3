% This record defines the structure of the client process.
% It contains the following fields:

%   gui: the name (or Pid) of the GUI process.
%	nick: assigned nickname to a client
%	connectedServer: server name(or Pid) of the server process. It initialize to zero. 
-record(cl_st, {nick, gui, connectedServer, connectedChannels}).

% This record defines the structure of the server process.
% It contains the following fields:
%	clients: The clients' names (or Pids) of the clients processes.
%	channels: The channels' names(or Pids) of the channels processes. 
%	name: name of server.
-record(server_st, {name, clients, channels, nicknames}).

%This record defines the structure of the channel process.
% It contains the following fields:
%	clients: The clients' names (or Pids) of the clients processes.
%	name: Name of channel(Chat room). 
-record(channel_st, {clients=[], name}).
