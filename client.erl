-module(client).
-export([main/1, initial_state/2]).
-include_lib("./defs.hrl").

%% Receive messages from GUI and handle them accordingly
main(State) ->
    receive
        {request, From, Ref, Request} ->
            {Response, NextState} = loop(State, Request),
            From ! {result, Ref, Response},
            main(NextState) 
    end.

%% Produce initial state
initial_state(Nick, GUIName) ->
    #cl_st{nick = Nick, gui = GUIName, connectedServer = zero, connectedChannels = []}.

%% ---------------------------------------------------------------------------

%% loop handles each kind of request from GUI
%%------------------
%% Connect to server. namnet loop ar bara en frivilig namn och kan vara vad som helst. Det syftar 
%% bara att client vantar alltid pa att fa en meddelande fran GUI och behandla den. 
%%-------------------------------------
%%When the helper:request function times out, the exit BIF is called. 
%%Look at Erlang's documentation for this BIF and you will see that it can be caught!:
%%> catch exit(foobar).
%%{'EXIT',foobar}

loop(St, {connect, Server}) ->
%%     if
%% 		Server =/= "shire" -> 
%% 			{{error, server_not_reached, "Please select shire server !"}, St};
%% 		true ->
		if
			 St#cl_st.connectedServer == Server -> 
	            {{error, user_already_connected, "You are already connected to the server."}, St};
	   		true -> 
			case 
				catch(helper:request(list_to_atom(Server), {connect, self(), St#cl_st.nick})) of
	            {'EXIT', {error, nickTaken, Msg}} ->	% Username have been taken by another user
	                {{error, user_already_connected, Msg, ":", nickTaken}, St};
	            {'EXIT', Reason} -> % if the server process is not available
	                {{error, server_not_reached, Reason}, St};
	            ok -> 
	                NewState = St#cl_st{connectedServer=Server}, 
	                {ok, NewState}
	        end
%% 		end
	end;

%% Disconnect from server
loop(St, disconnect) ->  
	if
        St#cl_st.connectedServer == zero -> 
            {{error, user_not_connected, "Where do you want to be disconnected from ?!!! :) "}, St};
        length(St#cl_st.connectedChannels) > 0 -> 
            {{error, leave_channels_first, "Leave all channels first then try again."}, St};
	true -> 
    case 
		catch(helper:request(list_to_atom(St#cl_st.connectedServer), {disconnect, self(), St#cl_st.nick})) of
	        {'EXIT', Reason} -> % if the server process is not available
	            {{error, server_not_reached, Reason}, St};
	        _Result -> 
	            NewState = St#cl_st{connectedServer=zero}, 
	            {ok, NewState}
	    end
    end;

% Join channel
loop(St, {join, Channel}) -> 	
	IsConnected = lists:member(Channel, St#cl_st.connectedChannels),
	if 
		IsConnected == true ->
			{{error, user_already_joined, "You has joined the channel already."}, St};
	true ->
	case
		catch(helper:request(list_to_atom(St#cl_st.connectedServer), {join, Channel, self()})) of
		{'EXIT', Reason} -> % if the server process is not available
                {{error, server_not_reached, Reason}, St};
		ok ->
			NewChannelList = St#cl_st.connectedChannels ++ [Channel],
			NewState = St#cl_st{connectedChannels=NewChannelList},
			{ok, NewState} 
		end
	end;

%% Leave channel by sending request to the channel.
%% In this way we prevent slowing down the flow of information in the server side.
loop(St, {leave, Channel}) ->
   IsConnected = lists:member(Channel, St#cl_st.connectedChannels), 	
	if
		IsConnected == false ->
			{{error, user_not_joined, "You haven't joind the chatroom"}, St};
        true -> 
			case catch (helper:request(list_to_atom(Channel),{leave, self()})) of 
			ok ->
				NewChannelList = lists:delete(Channel, St#cl_st.connectedChannels),
				NewState = St#cl_st{connectedChannels = NewChannelList},
				{ok, NewState};
			{'EXIT', Reason} -> % if the server process is not available
	        	{{error, server_not_reached, Reason}, St}
			end
    end;

% Sending messages
loop(St, {msg_from_GUI, Channel, Msg}) ->
    case 
		catch(helper:request(list_to_atom(Channel), {msgFromClient, self(), St#cl_st.nick, Msg})) of
		ok ->
			{ok, St};
		{error, user_not_joined} ->
			{{error, user_not_joined, "You haven't joind the chatroom"}, St};
		{error, user_not_connected} ->
			{{error, user_not_connected, "You are already connected to the server."}, St};
		{'EXIT', Reason} -> % if the server process is not available
	        {{error, server_not_reached, Reason}, St}
		
    end;

%% Get current nick
loop(St, whoami) ->
    {St#cl_st.nick, St} ;

%% Change nick
loop(St, {nick, Nick}) ->
	if
		St#cl_st.connectedServer == zero ->	%change nickname iff user is disconnected
		    NewState = St#cl_st{nick = Nick}, 
		    {ok, NewState} ;
		true ->
			{{error, user_already_connected, "You are already conneced to the server"}, St}
  	end;

%% Incoming message
loop(St = #cl_st { gui = GUIName }, {incoming_msg, Channel, Name, Msg}) ->
    gen_server:call(list_to_atom(GUIName), 
	{msg_to_GUI, Channel, Name++"> "++Msg}),
    {ok, St};

%%Ping 
loop(St, {ping, NicknameToPing}) ->
	if
		 St#cl_st.connectedServer == zero -> 
            {{error, user_not_connected, "You are not connected to the server"}, St};		 
   		true ->
			PingTimeStamp = now(),
			helper:requestAsync(list_to_atom(St#cl_st.connectedServer), {ping, self(), NicknameToPing, PingTimeStamp}), 
			{ok, St}
	end;

%%SendPong
loop(St, {sendPong, PidSender, NicknameReciever, PingTimeStamp}) ->
	if
		 St#cl_st.nick == NicknameReciever -> 
%% 			timer:sleep(1000),
            helper:requestAsync(PidSender, {pong, NicknameReciever, PingTimeStamp}),
			{ok, St};
   		true ->  
			{do_nothing, St}
	end;

%%Incoming Pong
loop(St = #cl_st { gui = GUIName }, {pong, NicknamePongSender, PingTimeStamp}) ->
	Diff = helper:timeSince(PingTimeStamp),
	gen_server:call(list_to_atom(GUIName), {msg_to_SYSTEM, io_lib:format("Pong ~s: ~pms", [NicknamePongSender,Diff])}),
    {ok, St}.
